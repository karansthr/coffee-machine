# Coffee machine

## Setup

### Prerequisite
- Python >= 3.9.0
- Make

#### Install poetry

Create and activate a virtual environment
```
python -m venv .venv && source .venv/bin/activate
```

Install poetry
```python
pip install poetry
```

#### Install dependencies
```sh
poetry install
```

#### Run unit tests
```sh
make test
```

#### Start coffee server
```sh
make serve
```

In debug mode:
```sh
LOGLEVEL=debug make serve
```

## Setup using Docker

### Prerequisite
- [Docker](https://docs.docker.com/engine/install/)
- [Docker Compose](https://docs.docker.com/compose/install/)

### Build image
```sh
$ docker-compose build
```

### Run unit tests
```sh
$ docker-compose run coffee_machine make test
```

### Start coffee server
```sh
$ docker-compose run coffee_machine make serve
```

## Configuration

#### Custom test payload
- To test a custom payload `PAYLOAD_PATH` environment variable can be set to path of test input.
```sh
docker-compose run coffee_machine make serve --env PAYLOAD_PATH=/app/tests/fixtures/test1.json
```

Following test cases can be used:
```sh
tests/fixtures
├── test1.json
├── test2.json
├── test3.json
├── test4.json
├── test5.json
└── test6.json
```

#### Ingredient threshold
Ingredient quantity threshold: when an ingredient is running low, the monitoring service logs it to stdout like
```
...
[alert] tea_leaves_syrup running low, please refill. remainig=40
...
```
A threshold of `50` is set by default, to override this `INGRIDIENT_QUANTITY_THRESHOLD` environment variable can be set.

Example:
```sh
docker-compose run coffee_machine make serve \
--env PAYLOAD_PATH=/app/tests/fixtures/test1.json \
--env INGRIDIENT_QUANTITY_THRESHOLD=100
```

Sample output
```sh
➜  coffee-machine git:(master) docker-compose run coffee_machine make serve \
--env PAYLOAD_PATH=/app/tests/fixtures/test1.json \
--env INGRIDIENT_QUANTITY_THRESHOLD=100

PAYLOAD_PATH=/app/tests/fixtures/test1.json poetry run python -m coffee_machine.app
----------
hot_tea is prepared
hot_coffee is prepared
black_tea cannot be prepared because item hot_water is not sufficient
green_tea cannot be prepared because item sugar_syrup is not sufficient
[alert] hot_milk running low, please refill. remainig=0
[alert] ginger_syrup running low, please refill. remainig=60
[alert] sugar_syrup running low, please refill. remainig=40
[alert] tea_leaves_syrup running low, please refill. remainig=40
```

#### Log level
By default, the log level is set to "info", but this can be updated by setting `LOGLEVEL` environment variable.

```sh
docker-compose run coffee_machine make serve \
--env PAYLOAD_PATH=/app/tests/fixtures/test1.json \
--env INGRIDIENT_QUANTITY_THRESHOLD=100 \
--env LOGLEVEL=debug
```

Sample output
```
➜  coffee-machine git:(master) ✗ docker-compose run coffee_machine make serve \
--env PAYLOAD_PATH=/app/tests/fixtures/test1.json \
--env INGRIDIENT_QUANTITY_THRESHOLD=100 \
--env LOGLEVEL=debug

PAYLOAD_PATH=/app/tests/fixtures/test1.json poetry run python -m coffee_machine.app
Monitoring coffee-machine, threshold=100
----------
Starting to prepare 'hot_tea'
consuming: acquired lock for hot_water
consuming: acquired lock for hot_milk
consuming: acquired lock for ginger_syrup
consuming: acquired lock for sugar_syrup
consuming: acquired lock for tea_leaves_syrup
hot_tea is prepared
Starting to prepare 'hot_coffee'
consuming: acquired lock for hot_water
consuming: acquired lock for ginger_syrup
consuming: acquired lock for hot_milk
consuming: acquired lock for sugar_syrup
consuming: acquired lock for tea_leaves_syrup
hot_coffee is prepared
Starting to prepare 'black_tea'
consuming: acquired lock for hot_water
black_tea cannot be prepared because item hot_water is not sufficient
Failed to prepare 'black_tea'
Starting to prepare 'green_tea'
consuming: acquired lock for hot_water
consuming: acquired lock for ginger_syrup
consuming: acquired lock for sugar_syrup
green_tea cannot be prepared because item sugar_syrup is not sufficient
Failed to prepare 'green_tea'
refilling: acquired lock for hot_water
refilling: acquired lock for ginger_syrup
```

LOGLEVEL can be set to one of the followings (case-insensitive):
- FATAL
- ERROR
- WARNING
- INFO
- DEBUG
