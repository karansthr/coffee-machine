import json
import logging
import os
import sys
from pathlib import Path

from coffee_machine import Dispenser
from coffee_machine import Ingredient
from coffee_machine import CoffeeMachine
from coffee_machine import MonitorMachine


LOGGER = logging.getLogger(__name__)


def main(path=os.getenv("PAYLOAD_PATH")):
    if path is None:
        LOGGER.error("'PAYLOAD_PATH' not specified.")
        sys.exit(1)

    path = Path(path)
    if not path.exists():
        LOGGER.error("No such file %s", path)
        sys.exit(1)
    
    with path.open("r") as fd:
        payload = json.load(fd)["machine"]

    machine = CoffeeMachine.from_config(
        {
            "outlets": int(payload["outlets"]["count_n"]),
            "ingredients": payload["total_items_quantity"]
        }
    )
    MonitorMachine(machine).start()
    dispenser = Dispenser(machine)
    dispenser.start()
    dispenser.request_beverages(payload["beverages"])


if __name__ == "__main__":
    main()
