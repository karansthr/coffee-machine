class CoffeeMachineError(Exception):
    """Parent exception class for all `CoffeeMachine` service errors"""


class IngredientDoesNotExist(CoffeeMachineError):
    """
    Raised when ingredient which is trying to be added
    does not exist
    """


class InsufficientIngredient(CoffeeMachineError):
    """
    Raised when ingredient is not available in requested
    quantity
    """
