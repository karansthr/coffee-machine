import logging
import threading
import time

from coffee_machine.config import INGRIDIENT_QUANTITY_THRESHOLD
from coffee_machine import Ingredient
from coffee_machine import CoffeeMachine

LOGGER = logging.getLogger(__name__)


class MonitorMachine(threading.Thread):

    POLL_STATUS_AFTER = 15  # seconds
    INGRIDIENT_QUANTITY_THRESHOLD = INGRIDIENT_QUANTITY_THRESHOLD

    def __init__(self, machine: CoffeeMachine):
        self.machine = machine
        super().__init__()

    def get_alerting_ingredients(self) -> list[Ingredient]:
        """
        Return a list of ingredients in the machine which are
        running low on quantity (i.e having its quantity less
        than `INGRIDIENT_QUANTITY_THRESHOLD`)
        """
        alerting_ingredients = []
        for ingredient in self.machine.ingredients.values():
            if ingredient.quantity < self.INGRIDIENT_QUANTITY_THRESHOLD:
                alerting_ingredients.append(ingredient)
        return alerting_ingredients

    def run(self):
        """
        Run the thread to monitor the coffee-machine ingredients
        periodically.
        """
        LOGGER.debug(
            "Monitoring coffee-machine, threshold=%s",
            self.INGRIDIENT_QUANTITY_THRESHOLD,
        )

        while True:
            alerting_ingredients = self.get_alerting_ingredients()
            for ingredient in alerting_ingredients:
                LOGGER.info(
                    "[alert] %s running low, please refill. remainig=%s",
                    ingredient.name,
                    ingredient.quantity,
                )
            LOGGER.info("-" * 10)
            time.sleep(self.POLL_STATUS_AFTER)
