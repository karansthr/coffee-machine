import logging
import queue
import threading
from typing import NamedTuple

from coffee_machine import CoffeeMachine
from coffee_machine.exceptions import (
    IngredientDoesNotExist, InsufficientIngredient
)

LOGGER = logging.getLogger("dispenser")


class Beverage(NamedTuple):
    name: str
    composition: dict


class Dispenser(threading.Thread):
    """beverage dispenser service"""
    def __init__(self, machine):
        self.machine = machine
        self.request_queue = queue.Queue(maxsize=machine.outlets)
        super().__init__()

    def prepare_beverage(self, name: str, composition: dict) -> bool:
        """
        Prepare the beverage with given composition. Returns a
        boolean indicating whether the beverage was prepared
        successfully or not.
        """
        LOGGER.debug("Starting to prepare '%s'", name)

        ingredients_loaded = {}  # will be helpful in rollback
        rollback = False

        # try to consume the ingredients from the machine -
        # while doing so, if there is atlease one ingridient
        # which is not avaiable in the given machine or it is
        # there is insufficeient quantity then beverage can't
        # be served

        for ingredient_name, quantity in composition.items():
            try:
                ingredient = self.machine.get_ingredient(ingredient_name)
            except IngredientDoesNotExist:
                rollback = True
                LOGGER.info(
                    "%s cannot be prepared because %s is not available",
                    name,
                    ingredient_name,
                )
                break

            try:
                self.machine.consume(ingredient, quantity)
            except InsufficientIngredient:
                rollback = True
                LOGGER.info(
                    "%s cannot be prepared because item %s is not sufficient",
                    name,
                    ingredient.name,
                )
                break
            else:
                ingredients_loaded[ingredient.name] = quantity

        # rollback variable is set when there is atleast one ingridient
        # which is either unavailable or its quantity is insufficient -
        # in which case start refilling the withdrawn ingriedients
        # because the beverage cannot be prepared.

        if not rollback:
            LOGGER.info("%s is prepared", name)
            return True
        
        LOGGER.debug("Failed to prepare '%s'", name)

        # start rollback
        for ingredient, quantity in ingredients_loaded.items():
            self.machine.refill(
                self.machine.get_ingredient(ingredient), quantity
            )
        
        return False

    def request_beverages(self, beverages: dict):
        """
        Request multiple beverages, only limited number of requests
        can be served concurrently (that limit would be the number
        of outlets the machine have.)
        """
        # This method put the beverages requests to request_queue
        threads = []
        for beverage_name, composition in beverages.items():
            beverage = Beverage(
                name=beverage_name, composition=composition
            )
            t = threading.Thread(
                target=self.request_queue.put, args=(beverage,)
            )
            t.start()
            threads.append(t)
        
        # join the threads
        for thread in threads:
            thread.join()

    def run(self):
        """Activate the dispenser: ready to serve the requests"""

        while True:
            bevearage = self.request_queue.get(block=True)
            self.prepare_beverage(bevearage.name, bevearage.composition)
