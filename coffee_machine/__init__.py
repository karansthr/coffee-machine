import logging
from os import getenv

if LOGLEVEL := getenv("LOGLEVEL"):
    LOGLEVEL = LOGLEVEL.upper()
else:
    LOGLEVEL = logging.INFO

logging.basicConfig(format='%(message)s', level=LOGLEVEL)

from .ingredient import Ingredient
from .machine import CoffeeMachine
from .dispenser import Dispenser
from .monitor import MonitorMachine
