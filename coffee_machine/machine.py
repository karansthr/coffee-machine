import logging
import threading
from typing import NamedTuple

from coffee_machine import Ingredient, exceptions

LOGGER = logging.getLogger(__name__)


class CoffeeMachine:
    """A coffee-machine service"""

    def __init__(self, outlets: int, ingredients: list[Ingredient]):
        self.outlets = outlets  # number of outlets
        self.ingredients = {
            ingredient.name: ingredient for ingredient in ingredients
        }

    def get_ingredient(self, name):
        """
        Return the `Ingredient` object given its name
        Raises:
            `IngredientDoesNotExist` if there is no such
            ingredient container in the machine.
        """
        if name not in self.ingredients:
            raise exceptions.IngredientDoesNotExist
        return self.ingredients[name]

    @classmethod
    def from_config(cls, config):
        """
        Factory method to get instance given the config in this format:
        {
            "outlets": <integer>,
            "ingredients": {
                "hot_water": 500,
                "hot_milk": 1000,
                ...
            }
        }
        """
        instance = cls(
            outlets=config["outlets"],
            ingredients=[
                Ingredient(name, quantity)
                for name, quantity in config["ingredients"].items()
            ]
        )
        return instance

    def refill(self, ingredient: Ingredient, quantity: int):
        """Add speicified qunatity a an ingredient container"""
        with ingredient._lock:
            LOGGER.debug("refilling: acquired lock for %s", ingredient.name)
            ingredient.quantity += quantity

    def consume(self, ingredient: Ingredient, quantity: int):
        """
        Consume specified quantity of a given ingredient
        Raises:
            `InsufficientIngredient` if ingredient quantity in
            not in sufficient quantity as requested.
        """
        with ingredient._lock:
            LOGGER.debug("consuming: acquired lock for %s", ingredient.name)
            if ingredient.quantity < quantity:
                raise exceptions.InsufficientIngredient
            ingredient.quantity -= quantity
