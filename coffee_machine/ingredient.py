from threading import Lock


class Ingredient:
    """Class to represent an ingredient"""

    def __init__(self, name: str, quantity: int, _lock=None):
        self.name = name
        self.quantity = quantity
        self._lock = _lock if _lock else Lock()
