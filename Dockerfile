FROM python:3.9.0-slim-buster

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

RUN pip install -U pip

RUN pip install poetry

RUN \
    apt-get update && \
    apt-get install -y make && \
    rm -rf /var/lib/apt/lists/*

RUN mkdir /app

WORKDIR /app

# install python dependencies first
COPY ./poetry.lock ./
COPY ./pyproject.toml ./
RUN poetry install --no-dev

# Copy the code
COPY . ./
