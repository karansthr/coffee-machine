import json
import threading

import pytest

from coffee_machine import CoffeeMachine, Ingredient, exceptions
from . import load_fixture


def test_coffee_machine():

    ingredients_info = {
        "hot_water": 500,
        "hot_milk": 500,
        "ginger_syrup": 100,
        "sugar_syrup": 100,
        "tea_leaves_syrup": 100
      }
    ingredients = []
    for ingredient_name, quantity in ingredients_info.items():
        ingredients.append(Ingredient(ingredient_name, quantity))

    machine = CoffeeMachine(5, ingredients)

    # test `get_ingredient` method
    ingredient = machine.get_ingredient("hot_water")
    assert ingredient

    with pytest.raises(exceptions.IngredientDoesNotExist):
        machine.get_ingredient("maggie_masala")

    # test `consume` and `refill`
    ingredient = machine.get_ingredient("hot_milk")
    machine.consume(ingredient, 400)  # 100 remains
    assert ingredient.quantity == 100

    with pytest.raises(exceptions.InsufficientIngredient):
        machine.consume(ingredient, 105)
    
    machine.refill(ingredient, 500)
    
    assert ingredient.quantity == 600  # 500 filled + 100 was already there

    threads = []
    for _ in range(500):
        t = threading.Thread(target=machine.consume, args=(ingredient, 1))
        t.start()  
        threads.append(t)
    
    for thread in threads:
        thread.join()
    
    assert ingredient.quantity == 100
