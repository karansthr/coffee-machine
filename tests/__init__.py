import json
from pathlib import Path

FIXTURE_PATH = Path(__file__).parent / "fixtures"


def load_fixture(name):
    with (FIXTURE_PATH / name).open("r") as fd:
        return json.load(fd)
