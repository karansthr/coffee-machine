from coffee_machine import CoffeeMachine, Ingredient, MonitorMachine


def test_monitor():
    ingredients_info = {
        "hot_water": 500,
        "hot_milk": 500,
        "ginger_syrup": 100,
        "sugar_syrup": 100,
        "tea_leaves_syrup": 100
      }
    ingredients = []
    for ingredient_name, quantity in ingredients_info.items():
        ingredients.append(Ingredient(ingredient_name, quantity))

    machine = CoffeeMachine(5, ingredients)

    monitor = MonitorMachine(machine)
    assert monitor.get_alerting_ingredients() == []

    monitor.INGRIDIENT_QUANTITY_THRESHOLD = 200
    alerting = monitor.get_alerting_ingredients()
    assert (
        sorted(["ginger_syrup", "sugar_syrup", "tea_leaves_syrup"])
        == sorted([alerting_ing.name for alerting_ing in alerting])
    )
