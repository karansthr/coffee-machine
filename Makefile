PAYLOAD_PATH=tests/fixtures/test1.json

serve:
	PAYLOAD_PATH=$(PAYLOAD_PATH) poetry run python -m coffee_machine.app

test:
	poetry run pytest
